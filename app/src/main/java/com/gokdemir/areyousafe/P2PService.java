package com.gokdemir.areyousafe;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceInfo;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceRequest;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by yatba on 14-Nov-17.
 */

public class P2PService  extends Service{
    private String TAG = "P2PService";
    final HashMap<String, String> buddies = new HashMap<String, String>();
    private final IntentFilter intentFilter = new IntentFilter();
    WifiP2pManager.Channel mChannel;
    private WifiP2pManager mManager;
    private static Integer user_num= 0;

    /*
         Registration is required to make service availabe for detection to other devices.
         by Hakan Yekta
      */
    private void discoverService() {
        WifiP2pManager.DnsSdTxtRecordListener txtListener = new WifiP2pManager.DnsSdTxtRecordListener() {
            @Override
            public void onDnsSdTxtRecordAvailable(String fullDomain, Map record, WifiP2pDevice device) {
                Log.i(TAG, "DnsSdTxtRecord available- " + record.toString());
                buddies.put("user"+new Integer(++user_num).toString(), device.deviceAddress);
            }
        };
        WifiP2pManager.DnsSdServiceResponseListener servListener = new WifiP2pManager.DnsSdServiceResponseListener() {
            @Override
            public void onDnsSdServiceAvailable(String instanceName, String registrationType,
                                                WifiP2pDevice resourceType) {
                Log.i(TAG,resourceType.deviceName);
                // Update the device name with the human-friendly version from
                // the DnsTxtRecord, assuming one arrived.
                resourceType.deviceName = buddies
                        .containsKey(resourceType.deviceAddress) ? buddies
                        .get(resourceType.deviceAddress) : resourceType.deviceName;
            }
        };
        mManager.setDnsSdResponseListeners(mChannel, servListener, txtListener);
        WifiP2pDnsSdServiceRequest serviceRequest = WifiP2pDnsSdServiceRequest.newInstance();
        mManager.addServiceRequest(mChannel,
                serviceRequest,
                new WifiP2pManager.ActionListener() {
                    @Override
                    public void onSuccess() {
                        // Success!
                        Log.d("Service Request","Succeed");
                    }

                    @Override
                    public void onFailure(int code) {
                        // Command failed.  Check for P2P_UNSUPPORTED, ERROR, or BUSY
                    }
                });
        mManager.discoverServices(mChannel, new WifiP2pManager.ActionListener() {

                    @Override
                    public void onSuccess() {
                        Log.d("Discovery", "Succeeded!");
                    }
                    @Override
                    public void onFailure(int code) {
                        // Command failed.  Check for P2P_UNSUPPORTED, ERROR, or BUSY
                        if (code == WifiP2pManager.P2P_UNSUPPORTED) {
                            Log.d("Failed", "P2P isn't supported on this device.");

                        }
                    }
                }
        );

        Log.d("Buddies",buddies.toString());
    }

    private void startRegistration() {
        Map record = new HashMap();
        record.put("buddyname","Mahmut");
        record.put("available", "visible");
        /*
        Service information.  Pass it an instance name, service type
        _protocol._transportlayer , and the map containing
        information other devices will want once they connect to this one.
        */
        WifiP2pDnsSdServiceInfo serviceInfo =
                WifiP2pDnsSdServiceInfo.newInstance("_test", "_presence._tcp", record);
        mManager.addLocalService(mChannel, serviceInfo, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                Log.i("GPS Tracker", "Successfully added the service!!");
            }

            @Override
            public void onFailure(int i) {
                Log.i("GPS Tracker", String.valueOf(i));
            }
        });
    }
    //--------------------------------------------------------------------------------------------------------------------------------
    //End
    @Override
    public void onCreate()
    {
        Log.i(TAG,"Started to P2P");
        mManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        mChannel = mManager.initialize(this, getMainLooper(), null);
        startRegistration();
        discoverService();
    }

    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }
}
