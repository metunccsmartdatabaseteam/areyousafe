package com.gokdemir.areyousafe;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import java.util.HashMap;

public class SessionManager {
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Context context;

    int PRIVATE_MODE = 0;

    //Shared Pref File Name
    private static final String PREF_NAME = "UserDetail";

    //All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";

    public static final String KEY_EMAIL = "email";

    public SessionManager(Context context){
        this.context = context;
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createLoginSession(String email){
        //Store login value as true.
        editor.putBoolean(IS_LOGIN, true);
        //Store email in preferences
        editor.putString(KEY_EMAIL, email);
        editor.commit();
    }

    public HashMap<String,String> getUserDetails(){
        HashMap<String, String> user = new HashMap<String, String>();

        //put email into the user object.
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL,null));

        return user;
    }

    public void checkLogin(){
        if(!this.isLoggedIn()){
            //User is not logged in redirect him to Login Activity
            Intent i = new Intent(context, MainActivity.class);

            //closing all the activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            //add new flag to start the new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            //starting the login activity
            context.startActivity(i);
        }
        else{
            Intent locationService = new Intent(context, Dashboard.class);
            locationService.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            locationService.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(locationService);
        }
    }

    public void logoutUser(){
        editor.clear();
        editor.commit();

        //after logout direct user to the login page.
        Intent i = new Intent(context, MainActivity.class);
        //close all the activities.
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        //add new flag to start the new activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        context.startActivity(i);
    }

    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN,false);
    }
}