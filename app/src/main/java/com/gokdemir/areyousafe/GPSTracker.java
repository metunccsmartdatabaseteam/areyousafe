package com.gokdemir.areyousafe;

/**
 * Created by gokde on 26.10.2017.
 */

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.icu.text.SimpleDateFormat;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.wifi.p2p.*;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceInfo;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceRequest;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Database;
import com.couchbase.lite.Document;
import com.couchbase.lite.Manager;
import com.couchbase.lite.Query;
import com.couchbase.lite.QueryEnumerator;
import com.couchbase.lite.QueryRow;
import com.couchbase.lite.UnsavedRevision;
import com.couchbase.lite.android.AndroidContext;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.net.ServerSocket;
import java.security.Provider;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class GPSTracker extends Service {
    private static int user_num = 0;
    private LocationListener listener;
    private LocationManager locationManager;
    private Timer timer = new Timer();
    private Database db;
    private String lat,lang;
    private String TAG = "GPS Tracker";
    private static final int LOCATION_INTERVAL = 1000 *40*1; // Every 3  Minutes
    private static final String addUserLocation = "http://smartdatabaseservice.azurewebsites.net/SmartDatabaseService.asmx/AddUserLocation";
    /*
        Required variables for to establish P2P Connection
        by Hakan Yekta
     */

    private final ScheduledExecutorService scheduler =
            Executors.newSingleThreadScheduledExecutor();

    private Future<?> timingTask;

    public void tick(long milliseconds) {
        timingTask = scheduler.scheduleAtFixedRate(new Runnable() {
            @SuppressLint("MissingPermission")
            public void run() {
                sendRequestToServer(locationManager.getLastKnownLocation(locationManager.NETWORK_PROVIDER));
                sendLocationToCouchBase(locationManager.getLastKnownLocation(locationManager.NETWORK_PROVIDER));
                try {
                    printLocalDB();
                } catch (CouchbaseLiteException e) {
                    e.printStackTrace();
                }
            }
        },0,milliseconds, TimeUnit.MILLISECONDS);
    }
    private boolean sendRequestToServer(Location l)
    {
        if(l != null) {
            Log.i(TAG,"Send Req");
            lat = String.valueOf(l.getLatitude());
            lang = String.valueOf(l.getLongitude());
            RequestQueue queue = Volley.newRequestQueue(this);
            StringRequest postRequest = new StringRequest(Request.Method.POST, addUserLocation,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response
                            Log.d("Response", response);

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.d("Error.Response", error.toString());
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {

                    SessionManager sessionManager = new SessionManager(getApplicationContext());
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String currentDateandTime = sdf.format(new Date());
                    Log.i(TAG,sessionManager.getUserDetails().toString());
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("mail", sessionManager.getUserDetails().get("email"));
                    params.put("xLoc", lat);
                    params.put("yLoc", lang);
                    params.put("t",currentDateandTime);
                    return parCY006-141797-0350ams;
                }
            };
            queue.add(postRequest);
        }
        else
        {
            Log.i(TAG,"NULL Location");
        }
        return true;
    }
    private boolean sendLocationToCouchBase(Location l)
    {
        if(l!=null)
        {
            try {
                lat = String.valueOf(l.getLatitude());
                lang = String.valueOf(l.getLongitude());
                Document document = db.getDocument("978-0061120053");
                document.update(new Document.DocumentUpdater() {
                    @Override
                    public boolean update(UnsavedRevision newRevision) {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
                        String currentDateandTime = sdf.format(new Date());
                        Log.i(TAG,currentDateandTime);
                        Map<String, Object> params =newRevision.getProperties();
                        params.put("Longitude", lang);
                        params.put("Latitude", lat);
                        params.put("Time Stamp",currentDateandTime);
                        newRevision.setUserProperties(params);

                        return true;
                    }
                });
            } catch (CouchbaseLiteException e) {
                Log.i("Local DB Management","Could not save the document");
                e.printStackTrace();
            }
            Log.i("Local DB Management","Sended to the local db");
        }
        return true;
    }
    private void printLocalDB() throws CouchbaseLiteException {

        Query query = db.createAllDocumentsQuery();
        query.setAllDocsMode(Query.AllDocsMode.ALL_DOCS);
        QueryEnumerator result = query.run();
        Log.i("Local DB Management",String.valueOf(result.getCount()));
        for (Iterator<QueryRow> it = result; it.hasNext(); ) {
            QueryRow row = it.next();
            Log.i("Local DB Management", String.valueOf(row.getDocument().getProperties()));
        }
    }
    //--------------------------------------------------------------------------------------------------------------------------------

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onCreate() {
        try{
            AndroidContext androidContext = new AndroidContext(getApplicationContext());
            Manager manager = new Manager(androidContext, Manager.DEFAULT_OPTIONS);
            db = manager.getExistingDatabase("safety");
            if (db == null) {
                db = manager.getDatabase("safety");
            }
        }catch(Exception e)  {
            e.printStackTrace();
        }
        listener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                /*Intent i = new Intent("location_update");
                i.putExtra("coordinates", location.getLongitude() + " " + location.getLatitude());
                sendBroadcast(i);
                */
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {
                Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }
        };

        locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        //noinspection MissingPermission
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }



        locationManager.requestLocationUpdates(locationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, 0, listener);

        tick(LOCATION_INTERVAL);
    }
    //--------------------------------------------------------------------------------------------------------------------------------
    @Override
    public void onDestroy() {
        super.onDestroy();
        if(locationManager != null){
            //noinspection MissingPermission
            locationManager.removeUpdates(listener);
        }
    }

}