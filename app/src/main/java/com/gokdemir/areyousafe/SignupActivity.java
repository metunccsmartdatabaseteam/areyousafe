package com.gokdemir.areyousafe;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class SignupActivity extends AppCompatActivity {

    private Button buttonSignup;
    private EditText editTextEmail, editTextPassword,editTextName;

    private ProgressDialog progressDialog;

    public static final String KEY_NAME = "name";
    public static final String KEY_EMAIL = "mail";
    public static final String KEY_PASSWORD = "password";
    private static final String addUserUrl = "http://smartdatabaseservice.azurewebsites.net/SmartDatabaseService.asmx/AddUser";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        buttonSignup = (Button) findViewById(R.id.buttonSignup);
        editTextName = (EditText) findViewById(R.id.etName);
        editTextEmail = (EditText) findViewById(R.id.etEmail);
        editTextPassword = (EditText) findViewById(R.id.etPassword);


        buttonSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String mail = editTextEmail.getText().toString();
                Log.i("mail",mail);
                final String password = editTextPassword.getText().toString().trim();
                final String name = editTextName.getText().toString().trim();
                Log.i("password", password);
                RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
                StringRequest postRequest = new StringRequest(Request.Method.POST, addUserUrl,
                        new Response.Listener<String>()
                        {
                            @Override
                            public void onResponse(String response) {
                                progressDialog.dismiss();
                                //display the response to the user...
                                if(response.toLowerCase().contains("false")){
                                    Toast.makeText(SignupActivity.this, "Could not register. Check your credentials.", Toast.LENGTH_LONG).show();
                                }
                                else{
                                    Toast.makeText(SignupActivity.this, "Successfully signed up.", Toast.LENGTH_SHORT).show();

                                    //direct the user to login page to enter the credentials...
                                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(i);
                                    finish();

                                    Toast.makeText(getApplicationContext(), "Please enter your credentials to login", Toast.LENGTH_SHORT).show();
                                }
                            }
                        },
                        new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                progressDialog.dismiss();
                                //displaying the error...
                                Toast.makeText(SignupActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                            }
                        }
                ) {
                    @Override
                    protected Map<String, String> getParams()
                    {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put(KEY_NAME,name);
                        params.put(KEY_EMAIL, mail);
                        params.put(KEY_PASSWORD, password);

                        return params;
                    }
                };
                queue.add(postRequest);
                progressDialog = new ProgressDialog(SignupActivity.this);
                progressDialog.setMessage("Signing you up. Please wait...");
                progressDialog.show();
            }
        });

    }



}