package com.gokdemir.areyousafe;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class SplashScreen extends AppCompatActivity {
    private TextView tv;
    private ImageView iv;

    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        sessionManager = new SessionManager(getApplicationContext());

        tv = (TextView) findViewById(R.id.sScreenTv);
        iv = (ImageView) findViewById(R.id.splashImage);

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.splash_transition);
        tv.startAnimation(animation);
        iv.startAnimation(animation);

        Thread timer = new Thread(){
            public void run(){
                try{
                    sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    sessionManager.checkLogin();
                }
            }
        };

        timer.start();
    }
}
