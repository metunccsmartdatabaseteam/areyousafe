package com.gokdemir.areyousafe;

import android.app.ProgressDialog;
import android.content.Intent;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;


public class MainActivity extends AppCompatActivity {
    private Button buttonLogin, buttonGoToSignup;
    private EditText etMail, etPassword;
    public static final String KEY_EMAIL = "mail";
    public static final String KEY_PASSWORD = "password";
    /*
        Need to create a XML tabşe for these key values!
     */
    private static final String isUserRegistered = "http://smartdatabaseservice.azurewebsites.net/SmartDatabaseService.asmx/IsRegistered";
    private static final String isPasswordCorrect = "http://smartdatabaseservice.azurewebsites.net/SmartDatabaseService.asmx/IsPasswordCorrect";

    public SessionManager session;
    public ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        session = new SessionManager(getApplicationContext());

        buttonLogin = (Button) findViewById(R.id.buttonLogin);
        buttonGoToSignup = (Button) findViewById(R.id.buttonGoToSignup);
        etMail = (EditText) findViewById(R.id.editTextLoginMail);
        etPassword = (EditText) findViewById(R.id.editTextLoginPassword);

        if(!isGpsEnabled()){
            Toast.makeText(MainActivity.this, "We need your location feature, please turn it on.", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
        }

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String mail = etMail.getText().toString();
                Log.i("mail", mail);
                final String password = etPassword.getText().toString();
                Log.i("password", password);

                if (mail.trim().length() > 0 && password.trim().length() > 0) {
                    RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, isPasswordCorrect,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response
                                    progressDialog.dismiss();
                                    if (response.toLowerCase().contains("false")) {
                                        Toast.makeText(MainActivity.this, "Could not login. Check your credentials.", Toast.LENGTH_LONG).show();
                                    } else if (response.toLowerCase().contains("true")) {
                                        Toast.makeText(MainActivity.this, "Successfully logged in.", Toast.LENGTH_LONG).show();

                                        session.createLoginSession(mail);

                                        Intent i = new Intent(getBaseContext(), Dashboard.class);
                                        startActivity(i);
                                    } else {
                                        Toast.makeText(MainActivity.this, "Something happened!", Toast.LENGTH_LONG).show();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    progressDialog.dismiss();
                                    Toast.makeText(MainActivity.this, error.getClass().getSimpleName(), Toast.LENGTH_SHORT).show();
                                }
                            }
                    ) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put(KEY_EMAIL, mail);
                            params.put(KEY_PASSWORD, password);

                            return params;
                        }
                    };
                    queue.add(postRequest);
                    progressDialog = new ProgressDialog(MainActivity.this);
                    progressDialog.setMessage("Logging in... Please wait.");
                    progressDialog.show();
                }
            }
        });
        buttonGoToSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), SignupActivity.class);
                startActivity(i);
            }
        });
    }

    private boolean isGpsEnabled()
    {
        LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
        return service.isProviderEnabled(LocationManager.GPS_PROVIDER)&&service.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

}